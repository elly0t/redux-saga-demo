import React, { Component, PropTypes } from 'react'

const PlaceholderCaller = ({ placeholder, getPlaceholder, cancelCurrentGet, getPlaceholderSlow, killAll }) =>
  <div>
    <button onClick={getPlaceholder}>
      Get new placeholder
    </button>
    <button onClick={cancelCurrentGet}>
      Manually Cancel get new placeholder
    </button>
    <button onClick={getPlaceholderSlow}>
      Get new placeholder (SLOW!)
    </button>
    <button onClick={killAll}>
      KILL ALL
    </button>
    <div>
      <hr />
      <img src={placeholder}/>
    </div>
  </div>

export default PlaceholderCaller

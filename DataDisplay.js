import React, { Component, PropTypes } from 'react'

const DataDisplay = ({ 
  totalPlaceholderCalls, 
  cancelledCalls
}) => 
  <div>
    <hr/>
    <div>total calls: {totalPlaceholderCalls}</div>
    <div>cancelled calls: {cancelledCalls}</div>
  </div>

export default DataDisplay
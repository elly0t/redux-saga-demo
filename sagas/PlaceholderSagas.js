import { randomNum } from '../utils'

import { 
  put, 
  takeLatest,
  take,
  fork,
  cancel,
  cancelled,
  all,
  race,
  call,
  spawn
} from 'redux-saga/effects'

import { delay } from 'redux-saga'

// constants
const DELAY = 2000
const LONG_DELAY = 10000
const TIME_OUT = 5000

export function * getPlaceholderQuickWatcher() {
  let counter = 0
  try {
    while (true) {
      yield take('GET_PLACEHOLDER')
      if (counter < 2) {
        console.log('counter: ', counter)
        const { success } = yield race({
          success: call(getPlaceholderQuick),
          timeOut: delay(TIME_OUT)
        })
        if (success) {
          counter += 1
        }
      } else {
        yield call(getSpecialPlaceholder)
        counter = 0
      }
    }
  } finally {
    if (yield cancelled()) {
      counter = 0
      console.log('getPlaceholderQuickWatcher killed')
    }
  }
}


// two types cancellation
// 1) killer
// 2) takeLatest
// this literally is takeLatest
export function * getPlaceholderSlowWatcher() {
  let lastPid
  try {
    while (true) {
      yield take('GET_PLACEHOLDER_SLOW')
      if (lastPid) {
        yield cancel(lastPid)
        yield put({type: 'UPDATE_CANCELLED_CALL'})
      }

      // lastPid = yield spawn(getPlaceholderSlow)
      lastPid = yield fork(getPlaceholderSlow)

      // cancel the forked task if it's still runnning after TIME_OUT
      yield delay(TIME_OUT)
      yield cancel(lastPid)
    }
  } finally {
    if (yield cancelled()) {
      console.log('getPlaceholderSlowWatcher killed')
    }
  }
}

export function * getPlaceholderQuick() {
  try {
    console.log('start of getPlaceholderQuick')
    yield delay(DELAY)
    yield call(getPlaceholder)
    return true
  } finally {
    if (yield cancelled()) {
      console.log('getPlaceholderQuick killed')
    }
  }
}

export function * getPlaceholderSlow() {
  try {
    console.log('start of getPlaceholderSlow')
    yield delay(LONG_DELAY)
    yield call(getPlaceholder)
  } finally {
    if (yield cancelled()) {
      console.log('getPlaceholderSlow killed')
    }
  }
}

export function * getSpecialPlaceholder() {
  yield put({type: 'UPDATE_PLACEHOLDER', placeholder: './resources/placeholder.gif'})
}

export function * getPlaceholder() {
  try {
    const w = randomNum(500, 300)
    const h = randomNum(500, 300)
    yield put({type: 'UPDATE_PLACEHOLDER', placeholder: `https://placekitten.com/${w}/${h}`})
  } finally {
    if (yield cancelled()) {
      console.log('someParentSaga killed')
    }
  }
}

// no going back
export function * murderAllSagas(pids) {
  yield take('KILL_ALL')
  yield cancel(...pids)
}

export default function * PlaceholderSagas() {
  const pid1 = yield fork(getPlaceholderQuickWatcher)
  const pid2 = yield fork(getPlaceholderSlowWatcher)
  yield fork(murderAllSagas, [pid1, pid2])
}

//use different commits!
// export default function * PlaceholderSagas() {
//   yield all([
//     getPlaceholderQuickWatcher(),
//     getPlaceholderSlowWatcher()
//   ])
// }

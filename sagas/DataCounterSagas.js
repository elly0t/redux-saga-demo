import { 
  put, 
  takeLatest,
  take,
  fork,
  cancel,
  cancelled,
  all,
  race,
  call
} from 'redux-saga/effects'

// take: function
export function * countTotalPlaceholderCall() {
  while(true) {
    yield take(action => action.type.includes('GET_PLACEHOLDER'))
    yield put({type: 'UPDATE_TOTAL_PLACEHOLDER_COUNTER'})
  }
}

export default function * DataCounterSagas() {
  yield all([
    countTotalPlaceholderCall()
  ])
}

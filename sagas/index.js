import { 
  put, 
  takeEvery,
  take,
  fork,
  cancel,
  cancelled,
  all,
  race,
  call
} from 'redux-saga/effects'

import PlaceholderSagas from './PlaceholderSagas'
import DataCounterSagas from './DataCounterSagas'

export default function * rootSaga() {
  yield all([
    PlaceholderSagas(),
    DataCounterSagas()
  ])
}
import "babel-polyfill"

import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootSagas from './sagas'

import PlaceholderCaller from './PlaceholderCaller'
import DataDisplay from './DataDisplay'
import rootReducers from './reduxStores'

// init saga
const sagaMiddleware = createSagaMiddleware()

// init redux store
const store = createStore(
  rootReducers,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(rootSagas)

const action = type => store.dispatch({type})

function render() {
  const { placeholderReducers, dataCounterReducers } = store.getState()
  const placeholder = placeholderReducers
  const {
    totalPlaceholderCalls,
    cancelledCalls
  } = dataCounterReducers

  return ReactDOM.render(
    <div>
      <PlaceholderCaller
        placeholder={placeholder}
        getPlaceholder={() => action('GET_PLACEHOLDER')}
        cancelCurrentGet={() => action('CANCEL_GET_PLACEHOLDER')}
        getPlaceholderSlow={() => action('GET_PLACEHOLDER_SLOW')}
        killAll={() => action('KILL_ALL')}
      />
      <DataDisplay
        totalPlaceholderCalls={totalPlaceholderCalls}
        cancelledCalls={cancelledCalls}
      />
    </div>,
    document.getElementById('root')
  )
}

render()
store.subscribe(render)

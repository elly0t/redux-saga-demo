const defaultPlaceholder = '../resources/default.jpg'

export default function placeholderReducers(state = defaultPlaceholder, action) {
  switch (action.type) {
    case 'UPDATE_PLACEHOLDER':
      return action.placeholder
    case 'CANCEL_GET_PLACEHOLDER':
    case 'GET_PLACEHOLDER':
    case 'GET_PLACEHOLDER_SLOW':
    case 'KILL_ALL':
    default:
      return state
  }
}

import { combineReducers } from 'redux'

import placeholderReducers from './PlaceholderRedux'
import dataCounterReducers from './DataCounterRedux'

export default combineReducers({
  placeholderReducers,
  dataCounterReducers
})
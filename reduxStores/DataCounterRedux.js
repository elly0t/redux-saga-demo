const initialStates = {
  totalPlaceholderCalls: 0,
  cancelledCalls: 0
}

export default function DataCounterRedcuers(state = initialStates, action) {
  switch (action.type) {
    case 'UPDATE_TOTAL_PLACEHOLDER_COUNTER':
      return {
        ...state,
        totalPlaceholderCalls: state.totalPlaceholderCalls + 1
      }
    case 'UPDATE_CANCELLED_CALL':    
      return {
        ...state,
        cancelledCalls: state.cancelledCalls + 1
      }
    default:
      return state
  }
}